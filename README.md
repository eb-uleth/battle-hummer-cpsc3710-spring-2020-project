# 🚀 BATTLE HUMMER 💥

## CPSC3710 Group Project

**Authors:** Everett Blakley and Logan Wilkie

The goal of this project is to create an interactive game using OpenGL, where the user drives around in a BATTLE HUMMER \*airhorn blasts directly into your ear\* 

------

## Build

First shoutout to [this](http://www.partow.net/programming/makefile/index.html) article for providing the skeleton for the Makefile for this project. There are several make commands that can be run

- `make` will compile, and run the default target, which is `all`
- `make all` will compile the program, and run it if successful
- `make clean` will remove any temporary files, and clean the output directories that aren't needed
- `make release` will compile the program, with extra compiler flags for release purposes
- `make debug` will compile the program, with extra compiler flags for debug purpose

The resulting executable(s) will be located within `build/output`

-----

## Develop

All implementation files should be put in `src` with the `.cpp` extension, and all specification files should be put in the `include` directory with the `.h` extension.

- If you are creating a subdirectory within `src`, add it to the Makefile `SRC` variable
  - Example: you're creating the `Building` class, with several subclasses (i.e. `PrismBuilding`, `SpericalBuilding`):
    1. Add the `Building.h`, `PrismBuilding.h`, and `SpericalBuilding.h` specification files to `include`
    2. Create a `building` subdirectory within `src`
    3. Add `Building.cpp`, `PrismBuilding.cpp`, and `SphericalBuilding.cpp` to the `building` subdirectory
    4. In the Makefile, add `$(wildcard src/building/*.cpp) \` to the next line under the `SRC` variable

-----

