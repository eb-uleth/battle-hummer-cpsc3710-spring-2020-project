/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#ifndef HUMMER_H
#define HUMMER_H

#include "Vector.h"
#include "Utils.h"
#include <cmath>
#include <GL/glut.h>

class Hummer
{
public:
  Vector position;
  Vector velocity;
  float angle;
  float movement = 0.2f;
  float rotation = 0.0f;
  float angleDelta = 1.0f;

  Hummer() : position(), velocity(), angle(180) {}
  Hummer(Vector pos) : position(pos), velocity(), angle(180) {}

  void move(int direction)
  {
    float rad = toRadians(angle - 90);
    if (direction == 1 || direction == -1)
    {
      if (compare(sin(rad), -1.0)) // Facing Pos-Z
      {
        if (direction == 1 && position.z != 140.0)
          position.z += 3.5;
        else if (direction == -1 && position.z != -140.0)
          position.z -= 3.5;
      }
      else if (compare(sin(rad), 1.0)) // Facing Neg-Z
      {
        if (direction == 1 && position.z != -140.0)
          position.z -= 3.5;
        else if (direction == -1 && position.z != 140.0)
          position.z += 3.5;
      }
      else if (compare(cos(rad), -1.0)) // Facing Neg-X
      {
        if (direction == 1 && position.x != -140.0)
          position.x -= 3.5;
        else if (direction == -1 && position.x != 140.0)
          position.x += 3.5;
      }
      else if (compare(cos(rad), 1.0)) // Facing Pos-X
      {
        if (direction == 1 && position.x != 140.0)
          position.x += 3.5;
        else if (direction == -1 && position.x != -140.0)
          position.x -= 3.5;
      }
    }
  }

  void update()
  {
    if (rotation != 0)
    {
      angle += rotation;
      rotation = 0;
      velocity *= 0.0f;
    }
    position += velocity;
    velocity *= 0.95;

    if (!compare(velocity.x, 0.0f) || !compare(velocity.z, 0.0f)) {
      tireAngle += 5;
    }

    outerAntennaRotate += outerAntennaSpeed;
    midAntennaRotate += midAntennaSpeed;
    innerAntennaRotate += innerAntennaSpeed;
    antennaRotate += antennaSpeed;
  }

  void draw();

private:
  float antennaRotate = 0.0;
  float antennaSpeed = 0.80;
  float outerAntennaRotate = 30.0;
  float outerAntennaSpeed = 1.35;
  float midAntennaRotate = 0.0;
  float midAntennaSpeed = 1.55;
  float innerAntennaRotate = 0.0;
  float innerAntennaSpeed = 2.05;
  float tireAngle = 0.0;
};

#endif
