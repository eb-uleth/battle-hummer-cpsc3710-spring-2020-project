/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
* Credit to: https://gist.github.com/DXsmiley/febd0ebd5f96d70576a0
*/

#ifndef VECTOR_H
#define VECTOR_H

#include <cmath>
#include <stdio.h>
#include "Utils.h"

class Vector
{
public:
  union {
    float data[3];
    struct
    {
      float x;
      float y;
      float z;
    };
  };

  // Constructors

  // Vectors default to 0, 0, 0.
  Vector()
  {
    x = 0;
    y = 0;
    z = 0;
  }

  // Construct with values, 3D
  Vector(float ax, float ay, float az)
  {
    x = ax;
    y = ay;
    z = az;
  }

  // Construct with values, 2D
  Vector(float ax, float az)
  {
    x = ax;
    y = 0.0;
    z = az;
  }

  // Construct a vector in 2D from angle
  Vector(float angle)
  {
    x = sin(toRadians(angle));
    z = cos(toRadians(angle));
    y = 0.0;
  }

  Vector(int angle)
  {
    x = sin(toRadians(angle));
    z = cos(toRadians(angle));
    y = 0.0;
  }

  // Copy constructor
  Vector(const Vector &o)
  {
    x = o.x;
    y = o.y;
    z = o.z;
  }

  // Addition

  Vector operator+(const Vector &o)
  {
    return Vector(x + o.x, y + o.y, z + o.z);
  }

  Vector &operator+=(const Vector &o)
  {
    x += o.x;
    y += o.y;
    z += o.z;
    return *this;
  }

  // Subtraction

  Vector operator-()
  {
    return Vector(-x, -y, -z);
  }

  Vector operator-(const Vector o)
  {
    return Vector(x - o.x, y - o.y, z - o.z);
  }

  Vector &operator-=(const Vector o)
  {
    x -= o.x;
    y -= o.y;
    z -= o.z;
    return *this;
  }

  // Multiplication by scalars

  Vector operator*(const float s)
  {
    return Vector(x * s, y * s, z * s);
  }

  Vector &operator*=(const float s)
  {
    x *= s;
    y *= s;
    z *= s;
    return *this;
  }

  // Division by scalars

  Vector operator/(const float s)
  {
    return Vector(x / s, y / s, z / s);
  }

  Vector &operator/=(const float s)
  {
    x /= s;
    y /= s;
    z /= s;
    return *this;
  }

  // Dot product

  float operator*(const Vector o)
  {
    return (x * o.x) + (y * o.y) + (z * o.z);
  }

  // An in-place dot product does not exist because
  // the result is not a vector.

  // Cross product

  Vector operator^(const Vector o)
  {
    float nx = y * o.z - o.y * z;
    float ny = z * o.x - o.z * x;
    float nz = x * o.y - o.x * y;
    return Vector(nx, ny, nz);
  }

  Vector &operator^=(const Vector o)
  {
    float nx = y * o.z - o.y * z;
    float ny = z * o.x - o.z * x;
    float nz = x * o.y - o.x * y;
    x = nx;
    y = ny;
    z = nz;
    return *this;
  }

  // Other functions

  // Length of Vectortor
  float magnitude()
  {
    return sqrt(magnitude_sqr());
  }

  // Length of vector squared
  float magnitude_sqr()
  {
    return (x * x) + (y * y) + (z * z);
  }

  // Returns a normalised copy of the vector
  // Will break if it's length is 0
  Vector normalised()
  {
    return Vector(*this) / magnitude();
  }

  // Modified the vector so it becomes normalised
  Vector &normalise()
  {
    (*this) /= magnitude();
    return *this;
  }

  void print(void)
  {
    printf("x: %.2f, y: %.2f, z: %.2f\n", x, y, z);
  }
};

#endif
