 /*
 * CPSC 3710 Spring 2020
 * Copyright 2019 Everett Blakley and Logan Wilkie
 * Everett Blakley <everett.blakley@uleth.ca>
 * Logan Wilkie <logan.wilkie@uleth.ca>
 */
 
 #ifndef STREETS_H
 #define STREETS_H

 #include <GL/gl.h>
 #include <GL/glut.h>
 #include <GL/glu.h>
 #include <vector>

 void drawStreets()
 {
    glColor3f(0.20, 0.20, 0.20);

    // Create base
    glBegin(GL_QUADS);
    // glColor3f(1.0, 0.0, 0.0);
    glVertex3i(-142, 0.0, -142);

    // glColor3f(0.0, 1.0, 0.0);
    glVertex3i(-142, 0.0, 142);

    // glColor3f(1.0, 0.0, 1.0);
    glVertex3i(142, 0.0, 142);
    
    // glColor3f(0.0, 0.0, 1.0);
    glVertex3i(142, 0.0, -142);
    glEnd();

    // Draw the Traffic Lines:
    glColor3f((247.0f/255.0f), (181.0f/255.0f), 0);
    glPushAttrib(GL_ENABLE_BIT);
    glLineStipple(4, 0xF0F0);
    glEnable(GL_LINE_STIPPLE);
        for (int i = 0; i <= 20; i++) {
        glBegin(GL_LINES);
            glVertex3f(-140 + (i * 14.0), 0.05, -140);
            glVertex3f(-140 + (i * 14.0), 0.05, 140);
    
            glVertex3f(-140, 0.05, -140 + (i * 14.0));
            glVertex3f(140, 0.05, -140 + (i * 14.0));
        glEnd();
        }
    glPopAttrib();
 }


 #endif // STREETS_H