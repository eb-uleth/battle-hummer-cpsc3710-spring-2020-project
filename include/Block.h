/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef BLOCK_H
#define BLOCK_H

#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <cstdlib>
#include <vector>

#include "Vector.h"
#include "Utils.h"
#include "Building.h"

using std::vector;


void drawBuilding(int type, float antennaRotation)
{
   if (type == 1)
      drawSquareBuilding();
   else if (type == 2)
      drawCylinderBuilding(antennaRotation);
   else
      drawSphereBuilding(2.0 * antennaRotation);
}

    // Draws a square block using the top left and bottom right (x, z) coordinate
void drawBlock(Vector topLeft, Vector bottomRight, std::vector<int> buildings, 
    std::vector<Vector> randomPositions, float spin, float elevation = 1.0) {
  glTranslatef(0.0f, elevation, 0.0f);

  // Top surface
  glColor3f(0.1f, 0.1f, 0.1f);
  glBegin(GL_QUADS);
  glVertex3f(bottomRight.x, 0.0f, bottomRight.z);
  glVertex3f(bottomRight.x, 0.0f, topLeft.z);
  glVertex3f(topLeft.x, 0.0f, topLeft.z);
  glVertex3f(topLeft.x, 0.0f, bottomRight.z);
  glEnd();
  
  glTranslatef(0.0f, -elevation, 0.0f);

  // Front surface
  glColor3f(0.125f, 0.125f, 0.125f);
  glBegin(GL_QUADS);
  glVertex3f(topLeft.x, elevation, bottomRight.z);
  glVertex3f(topLeft.x, 0.0f, bottomRight.z);
  glVertex3f(bottomRight.x, 0.0f, bottomRight.z);
  glVertex3f(bottomRight.x, elevation, bottomRight.z);
  glEnd();

  // Left surface
  glBegin(GL_QUADS);
  glVertex3f(topLeft.x, elevation, topLeft.z);
  glVertex3f(topLeft.x, 0.0f, topLeft.z);
  glVertex3f(topLeft.x, 0.0f, bottomRight.z);
  glVertex3f(topLeft.x, elevation, bottomRight.z);
  glEnd();

  // Back surface
  glBegin(GL_QUADS);
  glVertex3f(bottomRight.x, elevation, topLeft.z);
  glVertex3f(bottomRight.x, 0.0f, topLeft.z);
  glVertex3f(topLeft.x, 0.0f, topLeft.z);
  glVertex3f(topLeft.x, elevation, topLeft.z);
  glEnd();

  // Right surface
  glBegin(GL_QUADS);
  glVertex3f(bottomRight.x, elevation, bottomRight.z);
  glVertex3f(bottomRight.x, 0.0f, bottomRight.z);
  glVertex3f(bottomRight.x, 0.0f, topLeft.z);
  glVertex3f(bottomRight.x, elevation, topLeft.z);
  glEnd();

  float x_mid = (topLeft.x + bottomRight.x)/2.0f;
  float z_mid = (topLeft.z + bottomRight.z)/2.0f;


  for (unsigned int i = 0; i < buildings.size(); i++) {
    glLoadIdentity();
    glTranslatef(x_mid + randomPositions[i].x, elevation, z_mid + randomPositions[i].z);
    drawBuilding(buildings[i], spin); 
  }
}
#endif // BLOCK_H