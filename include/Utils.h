/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#ifndef UTILS_H
#define UTILS_H

#include <string.h>
#include <GL/glut.h>
#include <math.h>

#define PI 3.1415926535897932384626433832795

float toRadians(float angle);

void PrintString(void *font, char *str);

void drawCube(float scaleX = 1.0, float scaleY = 1.0, float scaleZ = 1.0);

void drawPyramid(void);

bool compare(float A, float B, float epsilon = 0.005f);

#endif