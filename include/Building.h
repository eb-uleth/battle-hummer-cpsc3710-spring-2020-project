/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#ifndef BUILDING_H
#define BUILDING_H

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

void drawSquareBuilding() {
   // Base **********************************************
   glColor3f(0.1, 0.1, 0.1);
   glBegin(GL_QUADS);
      glVertex3f(-1.0000,  0.0000,  1.0000);
      glVertex3f( 1.0000,  0.0000,  1.0000);
      glVertex3f( 1.0000,  6.0000,  1.0000);
      glVertex3f(-1.0000,  6.0000,  1.0000);

      glVertex3f(-1.0000,  0.0000,  1.0000);
      glVertex3f(-1.0000,  6.0000,  1.0000);
      glVertex3f(-1.0000,  6.0000, -1.0000);
      glVertex3f(-1.0000,  0.0000, -1.0000);

      glVertex3f(-1.0000,  0.0000, -1.0000);
      glVertex3f(-1.0000,  6.0000, -1.0000);
      glVertex3f( 1.0000,  6.0000, -1.0000);
      glVertex3f( 1.0000,  0.0000, -1.0000);

      glVertex3f( 1.0000,  0.0000,  1.0000);
      glVertex3f( 1.0000,  0.0000, -1.0000);
      glVertex3f( 1.0000,  6.0000, -1.0000);
      glVertex3f( 1.0000,  6.0000,  1.0000);

      glVertex3f(-1.0000,  6.0000, -1.0000);
      glVertex3f(-1.0000,  6.0000,  1.0000);
      glVertex3f( 1.0000,  6.0000,  1.0000);
      glVertex3f( 1.0000,  6.0000, -1.0000);  
   glEnd();

   glColor3f(0.76, 0.67, 0.1);
   glScalef(2.0, 1.0, 1.0);
   glTranslatef(0.0, 1.0, 0.0);
   glutSolidCube(1.015);

   glTranslatef(0.0, 2.0, 0.0);
   glScalef(0.5, 1.0, 2.0);
   glutSolidCube(1.015);
   
   glScalef(2.0, 1.0, 0.5);
   glTranslatef(0.0, 2.0, 0.0);
   glutSolidCube(1.015);

   glColor3f(0.0, 0.1, 0.85);
   glScalef(0.5, 1.0, 1.0);
   glTranslatef(0.0, -4.0, 0.0);
   glutWireCube(2.015);
   glTranslatef(0.0, 2.0, 0.0);
   glutWireCube(2.015);
   glTranslatef(0.0, 2.0, 0.0);
   glutWireCube(2.015);

   glTranslatef(0.0, 1.0, 0.0);
   glutWireCube(1.015);
   glColor3f(0.1, 0.1, 0.1);
   glutSolidCube(1.00);
}

void drawCylinderBuilding(float rotationAngle) {
   glColor3f(0.15, 0.15, 0.15);
   glRotatef(-90.0, 1.0, 0.0, 0.0);
   gluCylinder(gluNewQuadric(), 0.35, 0.35, 8.0, 30, 30);

   glColor3f(1.00, 0.0, 0.0);
   glTranslatef(0.0, 0.0, 8.0);
   glutWireSphere(0.80, 9, 40);
   
   glRotatef(rotationAngle * 0.75, 0.0, 0.0, 1.0);
   glutWireCube(1.5025);
   glColor3f(0.15, 0.15, 0.15);
   glutSolidCube(1.5);
}

void drawSphereBuilding(float rotationAngle) {
   glColor3f(0.1, 0.1, 0.1);
   glTranslatef(0.0, 2.0, 0.0);
   glutSolidSphere(2.0, 40, 40);

   glColor3f(0.0, 0.9, 0.0);
   glRotatef(90, 1.0, 0.0, 0.0);
   glutWireSphere(2.025, 30, 30);

   glTranslatef(0.0, 0.0, -1.5);
   glRotatef(rotationAngle, 0.0, 0.0, 1.0);
   glRotatef(210.0, 1.0, 0.0, 0.0);
   gluCylinder(gluNewQuadric(), 0.4, 0.4, 1.5, 20, 20);
}

#endif
