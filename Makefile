# Credit to http://www.partow.net/programming/makefile/index.html for the Makefile

CXX      := -g++
CXXFLAGS := -Wall -Wextra
LDFLAGS  := -L/usr/lib -lstdc++ -lm -lX11 -lXi -lXmu -lglut -lGL -lGLU
BUILD    := ./build
OBJ_DIR  := $(BUILD)/objects
APP_DIR  := $(BUILD)/output
TARGET   := program
INCLUDE  := -Iinclude/ -I/usr/include/
SRC      :=                      \
   $(wildcard src/*.cpp)         \
	 $(wildcard src/streets/*.cpp) \
	 $(wildcard src/hummer/*.cpp)  \

# Files to exclude from SRC to compile all
SRC := $(filter-out src/streets/streetsTest.cpp, $(SRC))
SRC := $(filter-out src/hummer/hummerTest.cpp, $(SRC))
SRC := $(filter-out src/viewing/viewingTest.cpp, $(SRC))
SRC := $(filter-out src/carTest.cpp, $(SRC))

# Classes? This seems weird to do but it works
UTILS  := src/Utils.cpp
HUMMER := src/hummer/Hummer.cpp

OBJECTS  := $(SRC:%.cpp=$(OBJ_DIR)/%.o)

all: build $(APP_DIR)/$(TARGET)
	$(APP_DIR)/$(TARGET)

$(OBJ_DIR)/%.o: %.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LDFLAGS)

$(APP_DIR)/$(TARGET): $(OBJECTS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(APP_DIR)/$(TARGET) $^ $(LDFLAGS)

# Target to build the main Battle Hummer program
BATTLEHUMMER_DIR     := src/battleHummer
BATTLEHUMMER_TARGET  := battleHummer
BATTLEHUMMER_SRC     := \
	$(wildcard $(BATTLEHUMMER_DIR)/*.cpp) \
	$(UTILS)

BATTLEHUMMER_OBJS    := $(BATTLEHUMMER_SRC:%.cpp=$(OBJ_DIR)/%.o)
$(APP_DIR)/$(BATTLEHUMMER_TARGET): $(BATTLEHUMMER_OBJS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(APP_DIR)/$(BATTLEHUMMER_TARGET) $^ $(LDFLAGS)

battleHummer: build $(APP_DIR)/$(BATTLEHUMMER_TARGET)
	$(APP_DIR)/$(BATTLEHUMMER_TARGET)

# Target to build the blocks program
BLOCKS_DIR     := src/block
BLOCKS_TARGET  := blocks
BLOCKS_SRC     := \
	$(wildcard $(BLOCKS_DIR)/*.cpp) \
	$(UTILS)

BLOCKS_OBJS    := $(BLOCKS_SRC:%.cpp=$(OBJ_DIR)/%.o)
$(APP_DIR)/$(BLOCKS_TARGET): $(BLOCKS_OBJS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(APP_DIR)/$(BLOCKS_TARGET) $^ $(LDFLAGS)

blocks: build $(APP_DIR)/$(BLOCKS_TARGET)
	$(APP_DIR)/$(BLOCKS_TARGET)

# Target to build the viewing test program
VIEWING_DIR 		:= src/viewing
VIEWING_TARGET 	:= viewing
VIEWING_SRC 		:= \
	$(wildcard $(VIEWING_DIR)/*.cpp) \
	$(UTILS) \
	$(HUMMER)
VIEWING_OBJS 		:= $(VIEWING_SRC:%.cpp=$(OBJ_DIR)/%.o)
$(APP_DIR)/$(VIEWING_TARGET): $(VIEWING_OBJS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(APP_DIR)/$(VIEWING_TARGET) $^ $(LDFLAGS)

viewing: build $(APP_DIR)/$(VIEWING_TARGET)
	$(APP_DIR)/$(VIEWING_TARGET)

# Target to build the hummer test program
HUMMER_DIR 		:= src/hummer
HUMMER_TARGET := hummer
HUMMER_SRC 		:= \
	$(wildcard $(HUMMER_DIR)/*.cpp) \
	$(UTILS)

HUMMER_OBJS 	:= $(HUMMER_SRC:%.cpp=$(OBJ_DIR)/%.o)
$(APP_DIR)/$(HUMMER_TARGET): $(HUMMER_OBJS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(APP_DIR)/$(HUMMER_TARGET) $^ $(LDFLAGS)

hummer: build $(APP_DIR)/$(HUMMER_TARGET)
	$(APP_DIR)/$(HUMMER_TARGET)

# Target to build the buildings test program
BUILDING_DIR 		:= src/buildings
BUILDING_TARGET := buildings
BUILDING_SRC 		:= \
	$(wildcard $(BUILDING_DIR)/*.cpp) \
	$(UTILS)

BUILDING_OBJS 	:= $(BUILDING_SRC:%.cpp=$(OBJ_DIR)/%.o)
$(APP_DIR)/$(BUILDING_TARGET): $(BUILDING_OBJS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(APP_DIR)/$(BUILDING_TARGET) $^ $(LDFLAGS)

buildings: build $(APP_DIR)/$(BUILDING_TARGET)
	$(APP_DIR)/$(BUILDING_TARGET)

.PHONY: all build clean debug release

build:
	@mkdir -p $(APP_DIR)
	@mkdir -p $(OBJ_DIR)

Debug: CXXFLAGS += -DDEBUG -g
Debug: all

Delease: CXXFLAGS += -O2
Release: all

clean:
	-@rm -rvf $(OBJ_DIR)/*
	-@rm -rvf $(APP_DIR)/*
