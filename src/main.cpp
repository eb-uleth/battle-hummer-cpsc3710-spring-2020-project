/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#define PROGRAM_TITLE "Everett and Logan - Battle Hummer!"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <time.h>
#include <cmath>

#include "Vector.h"
#include "Block.h"
#include "Utils.h"
#include "Hummer.h"
#include "Streets.h"

// using namespace std;
using std::vector;

//****** GLOBAL VARIABLES - DO NOT DELETE WITHOUT AGREEMENT ******************
static float fieldOfView = 45.0f;
static Vector eye = Vector(0.0, 2.0, 0.0);
static Vector lookat = Vector();
static Vector up = Vector({0.0, 1.0, 0.0});

int WindowWidth = 1200;
int WindowHeight = 1000;
int WindowID;

float AntennaRotation = 0.0;
float AntennaSpeed = 1.75;

bool paused = false;

vector<int> buildingTypes[400];
vector<Vector> randomPositions[400];

Hummer hummer = Hummer();
int defaultViewState = GLUT_KEY_F1;
int viewState = defaultViewState;

float offset = 5.0f;

bool displayDebugInformation = false;

void getViewState(void)
{
  switch (viewState)
  {
  /* Look Forward (default) */
  case GLUT_KEY_F1 || GLUT_KEY_F4:
    eye = Vector(hummer.position.x, 1.5f, hummer.position.z);
    lookat = (Vector(hummer.angle) * 100.0) + eye;
    up = Vector({0.0, 1.0, 0.0});
    break;
  /* Look right*/
  case GLUT_KEY_F2:
    eye = Vector(hummer.position.x, 2.0f, hummer.position.z);
    lookat = (Vector(hummer.angle - 90.0f) * 100.0) + eye;
    up = Vector({0.0, 1.0, 0.0});
    break;
  /* Look left */
  case GLUT_KEY_F3:
    eye = Vector(hummer.position.x, 2.0f, hummer.position.z);
    lookat = (Vector(hummer.angle + 90.0f) * 100.0) + eye;
    up = Vector({0.0, 1.0, 0.0});
    break;
  /* Look at truck from back right (near) */
  case GLUT_KEY_F5:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x + offset, offset, hummer.position.z + offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Look at truck from front right (near) */
  case GLUT_KEY_F6:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x + offset, offset, hummer.position.z - offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Look at truck from front left (near) */
  case GLUT_KEY_F7:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x - offset, offset, hummer.position.z - offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Look at truck from back left (near) */
  case GLUT_KEY_F8:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x - offset, offset, hummer.position.z + offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Look at truck from back right (far) */
  case GLUT_KEY_F9:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x + 3.0 * offset, 3.0 * offset, hummer.position.z + 3.0 * offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Look at truck from front right (far) */
  case GLUT_KEY_F10:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x + 3.0 * offset, 3.0 * offset, hummer.position.z - 3.0 * offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Look at truck from front left (far) */
  case GLUT_KEY_F11:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x - 3.0 * offset, 3.0 * offset, hummer.position.z - 3.0 * offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Look at truck from back left (far) */
  case GLUT_KEY_F12:
  {
    lookat = hummer.position;
    eye = Vector(hummer.position.x - 3.0 * offset, 3.0 * offset, hummer.position.z + 3.0 * offset);
    up = Vector({0.0, 1.0, 0.0});
    break;
  }
  /* Top down, for now */
  case 106:
  {
    lookat = hummer.position;
    up = Vector({0.0, 0.0, -1.0});
    eye = Vector({hummer.position.x, 20, hummer.position.z});
    break;
  }
  default:
    viewState = defaultViewState;
    break;
  }
}

void setProjection(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(fieldOfView, (GLfloat)WindowWidth / (GLfloat)WindowWidth, 0.1f, 100.0f);

  getViewState();

  gluLookAt(eye.x, eye.y, eye.z,
            lookat.x, lookat.y, lookat.z,
            up.x, up.y, up.z);
}

void drawDebugInformation(void)
{
  if (!displayDebugInformation)
  {
    return;
  }
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, WindowHeight, 0, WindowHeight, -1.0, 1.0);

  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  char lookatString[50];
  sprintf(lookatString, "Lookat: {%.2f, %.2f, %.2f}", lookat.x, lookat.y, lookat.z);
  glRasterPos2i(10, WindowHeight - 20);
  PrintString(GLUT_BITMAP_HELVETICA_12, lookatString);

  char eyeString[50];
  sprintf(eyeString, "Eye: {%.2f, %.2f, %.2f}", eye.x, eye.y, eye.z);
  glRasterPos2i(10, WindowHeight - 36);
  PrintString(GLUT_BITMAP_HELVETICA_12, eyeString);

  char fovString[30];
  sprintf(fovString, "Field of view: %.2f degrees", fieldOfView);
  glRasterPos2i(10, WindowHeight - 52);
  PrintString(GLUT_BITMAP_HELVETICA_12, fovString);

  char viewStateString[20];
  sprintf(viewStateString, "View State: %i", viewState);
  glRasterPos2i(10, WindowHeight - 68);
  PrintString(GLUT_BITMAP_HELVETICA_12, viewStateString);

  char truckPositionString[50];
  sprintf(truckPositionString, "Truck Position: {%.2f, %.2f, %.2f}", hummer.position.x, hummer.position.y, hummer.position.z);
  glRasterPos2i(10, WindowHeight - 84);
  PrintString(GLUT_BITMAP_HELVETICA_12, truckPositionString);

  char truckAngleString[150];
  sprintf(truckAngleString, "Truck Angle: %.2f degrees, sin(angle): %.1f, cos(angle): %1.f", hummer.angle, sin(toRadians(hummer.angle)), cos(toRadians(hummer.angle)));
  glRasterPos2i(10, WindowHeight - 100);
  PrintString(GLUT_BITMAP_HELVETICA_12, truckAngleString);

  char truckVelocityString[50];
  sprintf(truckVelocityString, "Truck Velocity: {%.2f, %.2f, %.2f}", hummer.velocity.x, hummer.velocity.y, hummer.velocity.z);
  glRasterPos2i(10, WindowHeight - 116);
  PrintString(GLUT_BITMAP_HELVETICA_12, truckVelocityString);

  glColor4f(1.0f, 1.0f, 1.0f, 0.2f);
  glTranslatef(0.0f, WindowHeight, 0.0f);
  glRotatef(-90, 0, 0, 1);
  glBegin(GL_QUADS);
  glVertex2i(0, 0);
  glVertex2i(0, 250);
  glVertex2i(120, 250);
  glVertex2i(120, 0);
  glEnd();
}

void display(void)
{
  setProjection();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // Draw the Streets: Map Base and Traffic Lines
  drawStreets();

  // Draw the Blocks
  glLoadIdentity();
  for (int i = 0; i < 20; i++)
  {
    for (int j = 0; j < 20; j++)
    {
      Vector v({-138 + (14 * i), -138 + (14 * j)});
      glLoadIdentity();
      drawBlock(v, {v.x + 10, v.z + 10},
                buildingTypes[i * 19 + j], randomPositions[i * 19 + j],
                AntennaRotation, 0.25);
    }
  }

  // Draw the Hummer
  glLoadIdentity();
  glTranslatef(hummer.position.x, 0.0, hummer.position.z);
  glRotatef(hummer.angle, 0, 1, 0);
  hummer.draw();

  // Handle Pausing
  if (paused)
  {
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WindowHeight, 0, WindowHeight, -1.0, 1.0);
    glColor4f(0.9f, 0.9f, 0.9f, 1.0f);
    char lookatString[70];
    sprintf(lookatString, "-Paused-      Press P to resume, ESC to quit");
    glRasterPos2i(5, 5);
    PrintString(GLUT_BITMAP_HELVETICA_18, lookatString);
  }

  drawDebugInformation();

  hummer.update();

  glFlush();
  glutSwapBuffers();

  AntennaRotation = fmod(AntennaRotation + AntennaSpeed, 360.0);
}

void mouseControl(int button, int state, int x, int y)
{
  int dummy = x; dummy += y;
  if (!paused)
  {
    switch (button)
    {
    case 3:
      if (state == GLUT_DOWN)
      {
        fieldOfView += 5.0f;
        printf("%f\n", fieldOfView);
      }
      break;
    case 4:
      if (state == GLUT_DOWN)
      {
        fieldOfView -= 5.0f;
        printf("%f\n", fieldOfView);
      }
      break;
    default:
      printf("Mouse clicked %d\n", button);
      break;
    }
  }
}
void keyboardControl(unsigned char key, int x, int y)
{
  int dummy = x; dummy += y;
  if (key == 27) // Esc: Close program
  {
    glutDestroyWindow(WindowID);
    exit(1);
  }

  if (key == 'p') // P: Pause the Game
  {
    paused = !paused;
    AntennaSpeed = 1.75 - AntennaSpeed;
  }

  if (key == 'd')
  {
    displayDebugInformation = !displayDebugInformation;
  }

  if (!paused)
  {
    switch (key)
    {
    case 97: // A: Drive the Hummer forwards
      hummer.move(1);
      break;
    case 113: // Q: Turn the Hummer to the Left
      hummer.rotation = 90.0;
      break;
    case 114: // R: Return the Hummer to the origin from the boundary
      if (hummer.position.x == -140.0 || hummer.position.x == 140.0 || hummer.position.z == -140.0 || hummer.position.z == 140.0)
      {
        hummer.position.x = 0.0;
        hummer.position.z = 0.0;
        hummer.angle = 180.0;
      }
      break;
    case 119: // W: Turn the Hummer to the Right
      hummer.rotation = -90.0;
      break;
    case 122: // Z: Drive the Hummer Backward
      hummer.move(-1);
      break;
    default:
      printf("Key pressed: %d\n", key);
      break;
    }
  }
}

void specialKeyControl(int key, int x, int y)
{
  int dummy = x; dummy += y;
  if (!paused)
  {
    float change = 3.5f;
    switch (key)
    {
    case GLUT_KEY_UP:
        hummer.move(1);
      // glutPostRedisplay();
      break;
    case GLUT_KEY_DOWN:
        hummer.move(-1);
      // glutPostRedisplay();
      break;
    case GLUT_KEY_LEFT:
      if (fmod(fabs(hummer.position.z), 14.0) == 0.0 && fmod(fabs(hummer.position.x), 14.0) == 0.0)
      {
        hummer.rotation = 90.0;
      }
      // glutPostRedisplay();
      break;
    case GLUT_KEY_RIGHT:
      if (fmod(fabs(hummer.position.z), 14.0) == 0.0 && fmod(fabs(hummer.position.x), 14.0) == 0.0)
      {
        hummer.rotation = -90.0;
      }
      // glutPostRedisplay();
      break;
    default:
      viewState = key;
      printf("Special key pressed: %d\n", key);
      break;
    }
  }
  getViewState();
}

void reshape(int w, int h)
{
  if (h == 0)
  {
    h = 1;
  }

  /* Set the view port */
  glViewport(0, 0, w, h);

  setProjection();
}

void init(void)
{
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glShadeModel(GL_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
}

int main(int argc, char **argv)
{
  srand(time(NULL));
  // Randomize Buildings
  for (int i = 0; i < 400; i++)
  {
    int nb = rand() % 4;
    for (int j = 0; j < nb; j++)
    {
      int bType = rand() % 3 + 1;
      buildingTypes[i].push_back(bType);
      int random_x = rand() % 4;
      if ((random_x % 2) == 0)
        random_x *= -1;
      int random_z = rand() % 4;
      if ((random_z % 2) == 0)
        random_z *= -1;
      randomPositions[i].push_back(Vector(random_x, random_z));
    }
  }

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(WindowWidth, WindowHeight);
  WindowID = glutCreateWindow(PROGRAM_TITLE);
  init();
  glutMouseFunc(mouseControl);
  glutSpecialFunc(&specialKeyControl);
  glutKeyboardFunc(&keyboardControl);

  glutDisplayFunc(display);
  glutIdleFunc(display);
  glutReshapeFunc(reshape);

  glutMainLoop();
  return 0; /* We'll never be here.*/
}
