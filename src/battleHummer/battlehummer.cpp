/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
* Logan Wilkie <logan.wilkie@uleth.ca>
*/

#define PROGRAM_TITLE "Everett and Logan - Battle Hummer!"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <time.h>

#include "Vector.h"
#include "Block.h"
// #include "Utils.h"

// using namespace std;
using std::vector;

//****** GLOBAL VARIABLES - DO NOT DELETE WITHOUT AGREEMENT ******************
static float fieldOfView = 45.0f;
static Vector eye = Vector(0.0, 1.0, 0.0);
static Vector lookat = Vector();

int WindowWidth = 1200;
int WindowHeight = 1000;

float AntennaRotation = 0.0;
float AntennaSpeed = 1.75;

bool paused = false;

vector<int> buildingTypes[400];
vector<Vector> randomPositions[400];

void setProjection(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(fieldOfView, (GLfloat)WindowWidth / (GLfloat)WindowWidth, 0.1f, 70.0f);
  gluLookAt(eye.x, eye.y, eye.z,
            eye.x, -5.0f, eye.z -50,
            0.0, 1.0, 0.0);
}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glColor3f(0.20, 0.20, 0.20);

  // Create base
  glBegin(GL_QUADS);
  glVertex3i(-142, 0.0, -142);
  glVertex3i(-142, 0.0, 142);
  glVertex3i(142, 0.0, 142);
  glVertex3i(142, 0.0, -142);
  glEnd();

  // Draw the Traffic Lines:
  glColor3f((247.0f/255.0f), (181.0f/255.0f), 0);
  glPushAttrib(GL_ENABLE_BIT);
  glLineStipple(4, 0xF0F0);
  glEnable(GL_LINE_STIPPLE);
    for (int i = 0; i <= 20; i++) {
      glBegin(GL_LINES);
        glVertex3f(-140 + (i * 14.0), 0.05, -140);
        glVertex3f(-140 + (i * 14.0), 0.05, 140);

        glVertex3f(-140, 0.05, -140 + (i * 14.0));
        glVertex3f(140, 0.05, -140 + (i * 14.0));
      glEnd();
    }
  glPopAttrib();

  // Draw the Blocks
  glLoadIdentity();
  for (int i = 0; i < 20; i ++) {
    for (int j = 0; j < 20; j++) {
      Vector v({-138 + (14 * i), -138 + (14 * j)});
      glLoadIdentity();
      drawBlock(v, {v.x + 10, v.z + 10},
                buildingTypes[i * 19 + j], randomPositions[i * 19 + j],
                AntennaRotation, 0.25);
    }
  }

  // Handle Pausing
  if (paused) {
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WindowHeight, 0, WindowHeight, -1.0, 1.0);
    glColor4f(0.9f, 0.9f, 0.9f, 1.0f);
    char lookatString[50];
    sprintf(lookatString, "-Paused-      Press P to resume");
    glRasterPos2i(5, 5);
    PrintString(GLUT_BITMAP_TIMES_ROMAN_24, lookatString);
  }

  glFlush();
  glutSwapBuffers();

  setProjection();

  AntennaRotation = fmod(AntennaRotation + AntennaSpeed, 360.0);
}

void mouseControl(int button, int state, int x, int y)
{
  if (!paused) {
    switch (button)
    {
      case 3:
        if (state == GLUT_DOWN)
        {
          fieldOfView += 5.0f;
          printf("%f\n", fieldOfView);
        }
        break;
      case 4:
        if (state == GLUT_DOWN)
        {
          fieldOfView -= 5.0f;
          printf("%f\n", fieldOfView);
        }
        break;
    default:
      printf("Mouse clicked %d\n", button);
      break;
    }
  }
}

void keyboardControl(unsigned char key, int x, int y)
{
  if (key == 'p') {
    paused = !paused;
    AntennaSpeed = 1.75 - AntennaSpeed;
  }

  if (!paused) {
    switch (key)
    {
      case 114:
        if (eye.x == -140.0 || eye.x == 140.0 || eye.z == -140.0 || eye.z == 140.0) {
          eye.x = 0.0;
          eye.z = 0.0;
        }
      break;
    default:
      printf("Key pressed: %d\n", key);
      break;
    }
  }
}


void specialKeyControl(int key, int x, int y)
{
  if (!paused) {
    float change = 3.5f;
    switch (key)
    {
    case GLUT_KEY_UP:
      if (fmod(fabs(eye.x), 14.0) == 0.0 && eye.z > -140) {
          eye.z -= change;
      }
      glutPostRedisplay();
      break;
    case GLUT_KEY_DOWN:
      if (fmod(fabs(eye.x), 14.0) == 0.0 && eye.z < 140) {
          eye.z += change;
      }
      glutPostRedisplay();
      break;
    case GLUT_KEY_LEFT:
      if (fmod(fabs(eye.z), 14.0) == 0.0 && eye.x > -140) {
          eye.x -= change;
      }
      glutPostRedisplay();
      break;
    case GLUT_KEY_RIGHT:
      if (fmod(fabs(eye.z), 14.0) == 0.0 && eye.x < 140) {
          eye.x += change;
      }

      glutPostRedisplay();
      break;
    default:
      printf("Special key pressed: %d\n", key);
      break;
    }
  }
}


void reshape(int w, int h)
{
  if (h == 0)
  {
    h = 1;
  }

  /* Set the view port */
  glViewport(0, 0, w, h);

  setProjection();
}


void init(void)
{
  glClearColor(0.0f, 0.0f, 0.075f, 1.0f);
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glShadeModel(GL_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
}

int main(int argc, char **argv)
{
  srand(time(NULL));
  // Randomize Buildings
  for (int i = 0; i < 400; i++)
  {
    int nb = rand() % 4;
    for (int j = 0; j < nb; j++)
    {
      int bType = rand() % 3 + 1;
      buildingTypes[i].push_back(bType);
      int random_x = rand() % 4;
      if ((random_x % 2) == 0)
        random_x *= -1;
      int random_z = rand() % 4;
      if ((random_z % 2) == 0)
        random_z *= -1;
      randomPositions[i].push_back(Vector(random_x, random_z));
    }
  }

  glutInit(&argc, argv);
  int initial_width = glutGet(GLUT_WINDOW_WIDTH) | WindowWidth;
  int initial_height = glutGet(GLUT_WINDOW_HEIGHT) | WindowHeight;
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(initial_width, initial_height);
  glutCreateWindow(PROGRAM_TITLE);

  init();
  glutDisplayFunc(display);
  glutIdleFunc(display);
  glutReshapeFunc(reshape);

  glutMouseFunc(&mouseControl);
  glutSpecialFunc(&specialKeyControl);
  glutKeyboardFunc(&keyboardControl);

  glutMainLoop();
  return 0; /* We'll never be here.*/
}
