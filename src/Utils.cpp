/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include <Utils.h>

float toRadians(float angle)
{
  return angle * PI / 180.0;
}

void PrintString(void *font, char *str)
{
  int i, len = strlen(str);

  for (i = 0; i < len; i++)
    glutBitmapCharacter(font, *str++);
}

void drawCube(float scaleX, float scaleY, float scaleZ)
{
  glScalef(scaleX, scaleY, scaleZ);
  glBegin(GL_QUADS); // Begin drawing the color cube with 6 quads
  // Top face (y = 1.0f)
  // Define vertices in counter-clockwise (CCW) order with normal pointing out
  glColor3f(0.5f, 0.5f, 0.5f); // Grey
  glVertex3f(1.0f, 1.0f, -1.0f);
  glVertex3f(-1.0f, 1.0f, -1.0f);
  glVertex3f(-1.0f, 1.0f, 1.0f);
  glVertex3f(1.0f, 1.0f, 1.0f);

  // Bottom face (y = -1.0f)
  glColor3f(1.0f, 0.5f, 0.0f); // Orange
  glVertex3f(1.0f, -1.0f, 1.0f);
  glVertex3f(-1.0f, -1.0f, 1.0f);
  glVertex3f(-1.0f, -1.0f, -1.0f);
  glVertex3f(1.0f, -1.0f, -1.0f);

  // Front face  (z = 1.0f)
  glColor3f(1.0f, 0.0f, 0.0f); // Red
  glVertex3f(1.0f, 1.0f, 1.0f);
  glVertex3f(-1.0f, 1.0f, 1.0f);
  glVertex3f(-1.0f, -1.0f, 1.0f);
  glVertex3f(1.0f, -1.0f, 1.0f);

  // Back face (z = -1.0f)
  glColor3f(1.0f, 1.0f, 0.0f); // Yellow
  glVertex3f(1.0f, -1.0f, -1.0f);
  glVertex3f(-1.0f, -1.0f, -1.0f);
  glVertex3f(-1.0f, 1.0f, -1.0f);
  glVertex3f(1.0f, 1.0f, -1.0f);

  // Left face (x = -1.0f)
  glColor3f(0.0f, 0.0f, 1.0f); // Blue
  glVertex3f(-1.0f, 1.0f, 1.0f);
  glVertex3f(-1.0f, 1.0f, -1.0f);
  glVertex3f(-1.0f, -1.0f, -1.0f);
  glVertex3f(-1.0f, -1.0f, 1.0f);

  // Right face (x = 1.0f)
  glColor3f(1.0f, 0.0f, 1.0f); // Magenta
  glVertex3f(1.0f, 1.0f, -1.0f);
  glVertex3f(1.0f, 1.0f, 1.0f);
  glVertex3f(1.0f, -1.0f, 1.0f);
  glVertex3f(1.0f, -1.0f, -1.0f);
  glEnd(); // End of drawing color-cube
}

void drawPyramid(void)
{
  glBegin(GL_TRIANGLES); // Begin drawing the pyramid with 4 triangles
  // Front
  glColor3f(1.0f, 0.0f, 0.0f); // Red
  glVertex3f(0.0f, 1.0f, 0.0f);
  glColor3f(0.0f, 1.0f, 0.0f); // Green
  glVertex3f(-1.0f, -1.0f, 1.0f);
  glColor3f(0.0f, 0.0f, 1.0f); // Blue
  glVertex3f(1.0f, -1.0f, 1.0f);

  // Right
  glColor3f(1.0f, 0.0f, 0.0f); // Red
  glVertex3f(0.0f, 1.0f, 0.0f);
  glColor3f(0.0f, 0.0f, 1.0f); // Blue
  glVertex3f(1.0f, -1.0f, 1.0f);
  glColor3f(0.0f, 1.0f, 0.0f); // Green
  glVertex3f(1.0f, -1.0f, -1.0f);

  // Back
  glColor3f(1.0f, 0.0f, 0.0f); // Red
  glVertex3f(0.0f, 1.0f, 0.0f);
  glColor3f(0.0f, 1.0f, 0.0f); // Green
  glVertex3f(1.0f, -1.0f, -1.0f);
  glColor3f(0.0f, 0.0f, 1.0f); // Blue
  glVertex3f(-1.0f, -1.0f, -1.0f);

  // Left
  glColor3f(1.0f, 0.0f, 0.0f); // Red
  glVertex3f(0.0f, 1.0f, 0.0f);
  glColor3f(0.0f, 0.0f, 1.0f); // Blue
  glVertex3f(-1.0f, -1.0f, -1.0f);
  glColor3f(0.0f, 1.0f, 0.0f); // Green
  glVertex3f(-1.0f, -1.0f, 1.0f);
  glEnd(); // Done drawing the pyramid
}

bool compare(float A, float B, float epsilon)
{
  return (fabs(A-B) < epsilon);
}