/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#define PROGRAM_TITLE "Viewing Test"
#define NUMBER_OF_OBJECTS 10

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <time.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>

#include "Utils.h"
#include "Vector.h"
#include "Hummer.h"

using namespace std;

static Vector randomPositions[NUMBER_OF_OBJECTS];

int GroundSize = 20;

int WindowWidth = 1000;
int WindowHeight = 1000;

Hummer hummer = Hummer();

static float fieldOfView = 45.0f;
static Vector eyePosition;
static Vector lookatPosition;

int defaultViewState = GLUT_KEY_F1;
int viewState = defaultViewState;

bool displayDebugInformation = false;

void getViewState(void)
{
  switch (viewState)
  {
  /* Look Forward (default) */
  case GLUT_KEY_F1 || GLUT_KEY_F4:
    eyePosition = Vector(hummer.position.x, 3.0f, hummer.position.z);
    lookatPosition = (Vector(hummer.angle) * -100) + eyePosition;
    break;
  /* Look right*/
  case GLUT_KEY_F2:
    eyePosition = Vector(hummer.position.x, 2.0f, hummer.position.z);
    lookatPosition = (Vector(hummer.angle + 90) * 100) + eyePosition;
    break;
  /* Look left */
  case GLUT_KEY_F3:
    eyePosition = Vector(hummer.position.x, 2.0f, hummer.position.z);
    lookatPosition = (Vector(hummer.angle - 90) * 100) + eyePosition;
    break;
  /* Look at truck from back right (near) */
  case GLUT_KEY_F5:
  {
    lookatPosition = hummer.position;
    Vector angle = Vector(hummer.angle);
    eyePosition = Vector(hummer.position.x * angle.x - 20.0f, 20.0f, hummer.position.z * angle.z - 20.0f);
    break;
  }
  /* Look at truck from front right (near) */
  case GLUT_KEY_F6:
    break;
  /* Look at truck from front left (near) */
  case GLUT_KEY_F7:
    break;
  /* Look at truck from back left (near) */
  case GLUT_KEY_F8:
    break;
  /* Look at truck from back right (far) */
  case GLUT_KEY_F9:
    break;
  /* Look at truck from front right (far) */
  case GLUT_KEY_F10:
    break;
  /* Look at truck from front left (far) */
  case GLUT_KEY_F11:
    break;
  /* Look at truck from back left (far) */
  case GLUT_KEY_F12:
    break;
  default:
    viewState = defaultViewState;
    break;
  }
}

void setProjection(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(fieldOfView, (GLfloat)WindowWidth / (GLfloat)WindowWidth, 0.1f, 100.0f);

  getViewState();

  gluLookAt(eyePosition.x, eyePosition.y, eyePosition.z,
            lookatPosition.x, lookatPosition.y, lookatPosition.z,
            0.0, 1.0, 0.0);
}

void drawDebugInformation(void)
{
  if (!displayDebugInformation)
  {
    return;
  }
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, WindowHeight, 0, WindowHeight, -1.0, 1.0);

  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  char lookatString[50];
  sprintf(lookatString, "Lookat: {%.2f, %.2f, %.2f}", lookatPosition.x, lookatPosition.y, lookatPosition.z);
  glRasterPos2i(10, WindowHeight - 20);
  PrintString(GLUT_BITMAP_HELVETICA_12, lookatString);

  char eyeString[50];
  sprintf(eyeString, "Eye: {%.2f, %.2f, %.2f}", eyePosition.x, eyePosition.y, eyePosition.z);
  glRasterPos2i(10, WindowHeight - 36);
  PrintString(GLUT_BITMAP_HELVETICA_12, eyeString);

  char fovString[30];
  sprintf(fovString, "Field of view: %.2f degrees", fieldOfView);
  glRasterPos2i(10, WindowHeight - 52);
  PrintString(GLUT_BITMAP_HELVETICA_12, fovString);

  char viewStateString[20];
  sprintf(viewStateString, "View State: %i", viewState);
  glRasterPos2i(10, WindowHeight - 68);
  PrintString(GLUT_BITMAP_HELVETICA_12, viewStateString);

  char truckPositionString[50];
  sprintf(truckPositionString, "Truck Position: {%.2f, %.2f, %.2f}", hummer.position.x, hummer.position.y, hummer.position.z);
  glRasterPos2i(10, WindowHeight - 84);
  PrintString(GLUT_BITMAP_HELVETICA_12, truckPositionString);

  char truckAngleString[50];
  sprintf(truckAngleString, "Truck Angle: %.2f degrees", hummer.angle);
  glRasterPos2i(10, WindowHeight - 100);
  PrintString(GLUT_BITMAP_HELVETICA_12, truckAngleString);

  char truckVelocityString[50];
  sprintf(truckVelocityString, "Truck Velocity: {%.2f, %.2f, %.2f}", hummer.velocity.x, hummer.velocity.y, hummer.velocity.z);
  glRasterPos2i(10, WindowHeight - 116);
  PrintString(GLUT_BITMAP_HELVETICA_12, truckVelocityString);

  glColor4f(1.0f, 1.0f, 1.0f, 0.2f);
  glTranslatef(0.0f, WindowHeight, 0.0f);
  glRotatef(-90, 0, 0, 1);
  glBegin(GL_QUADS);
  glVertex2i(0, 0);
  glVertex2i(0, 250);
  glVertex2i(120, 250);
  glVertex2i(120, 0);
  glEnd();
}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glColor3f(0.1, 0.6, 0.2);

  glBegin(GL_QUADS);
  glVertex3i(-GroundSize, 0, GroundSize);
  glVertex3i(GroundSize, 0, GroundSize);
  glVertex3i(GroundSize, 0, -GroundSize);
  glVertex3i(-GroundSize, 0, -GroundSize);
  glEnd();

  glLoadIdentity();
  glTranslatef(hummer.position.x, 1.0, hummer.position.z);
  glRotatef(-hummer.angle, 0, 1, 0);
  hummer.draw();

  for (int i = 0; i < 5; i++)
  {
    glLoadIdentity();
    glTranslatef(randomPositions[i].x, 1.0f, randomPositions[i].z);
    drawCube();
  }

  for (int i = 5; i < 10; i++)
  {
    glLoadIdentity();
    glTranslatef(randomPositions[i].x, 1.0f, randomPositions[i].z);
    drawPyramid();
  }

  drawDebugInformation();

  glFlush();
  glutSwapBuffers();

  setProjection();
  hummer.update();
}

void init(void)
{
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);              // Set background color to black and opaque
  glClearDepth(1.0f);                                // Set background depth to farthest
  glEnable(GL_DEPTH_TEST);                           // Enable depth testing for z-culling
  glDepthFunc(GL_LESS);                              // Set the type of depth-test
  glShadeModel(GL_SMOOTH);                           // Enable smooth shading
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Nice perspective corrections
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  /* Generate some random coordinates */
  for (int i = 0; i < NUMBER_OF_OBJECTS; i++)
  {
    float randomX = rand() % (GroundSize * 2) - GroundSize + 1;
    float randomZ = rand() % (GroundSize * 2) - GroundSize + 1;
    randomPositions[i] = {randomX, randomZ};
  }
}

void mouseControl(int button, int state, int x, int y)
{
  switch (button)
  {
  case 3:
    if (state == GLUT_DOWN)
    {
      fieldOfView += 1.0f;
    }
    break;
  case 4:
    if (state == GLUT_DOWN)
    {
      fieldOfView -= 1.0f;
    }
    break;
  default:
    printf("Mouse clicked %d %d\n", button, state);
    break;
  }
}

void specialKeyControl(int key, int x, int y)
{
  switch (key)
  {
  case GLUT_KEY_UP:
    hummer.move(-1);
    break;
  case GLUT_KEY_DOWN:
    hummer.move(1);
    break;
  case GLUT_KEY_LEFT:
    hummer.rotation = -90;
    break;
  case GLUT_KEY_RIGHT:
    hummer.rotation = 90;
    break;
  default:
    // printf("Special key pressed: %d\n", key);
    viewState = key;
    break;
  }
}

void keyboardControl(unsigned char key, int x, int y)
{
  switch (key)
  {
  /* D Key: Display Debug information */
  case 100:
    displayDebugInformation = !displayDebugInformation;
    break;
  default:
    printf("Key pressed: %d\n", key);
    break;
  }
}

void reshape(int w, int h)
{
  if (h == 0)
  {
    h = 1;
  }

  /* Set the view port */
  glViewport(0, 0, w, h);

  WindowWidth = w;
  WindowHeight = h;

  setProjection();
}

int main(int argc, char **argv)
{

  glutInit(&argc, argv);
  int initial_width = glutGet(GLUT_WINDOW_WIDTH) | WindowWidth;
  int initial_height = glutGet(GLUT_WINDOW_HEIGHT) | WindowHeight;

  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(initial_width, initial_height);
  glutCreateWindow(PROGRAM_TITLE);

  init();
  glutReshapeFunc(reshape);
  glutDisplayFunc(display);
  glutIdleFunc(display);

  glutMouseFunc(&mouseControl);
  glutSpecialFunc(&specialKeyControl);
  glutKeyboardFunc(&keyboardControl);

  glutMainLoop();
  return 0; /* We'll never be here.*/
}
