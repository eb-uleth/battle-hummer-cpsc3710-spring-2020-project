/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#define PROGRAM_TITLE "Streets Test"

#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <time.h>

#include "Streets.h"
#include "Vector.h"
#include "Block.h"

using namespace std;

static float fieldOfView = 45.0f;
static Vector eye = Vector(-20.0, 20.0, -20.0);
static Vector lookat = Vector();

int WindowWidth = 1000;
int WindowHeight = 1000;

int BlockSize = 5;

int GroundSize = 20;

vector<float> xCoords({-20.0f, -10.0f, 0.0f, 10.0f, 20.0f});
vector<float> zCoords({-20.0f, -10.0f, 0.0f, 20.0f});

vector<int> buildingTypes[5];
vector<Vector> randomPositions[5];

void setProjection(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(fieldOfView, (GLfloat)WindowWidth / (GLfloat)WindowWidth, 0.1f, 100.0f);

  gluLookAt(eye.x, eye.y, eye.z,
            lookat.x, lookat.y, lookat.z,
            0.0, 1.0, 0.0);
}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glColor4f(0.1, 0.1, 0.1, 0.5);

  glBegin(GL_QUADS);
  glVertex3i(-GroundSize, 0, GroundSize);
  glVertex3i(GroundSize, 0, GroundSize);
  glVertex3i(GroundSize, 0, -GroundSize);
  glVertex3i(-GroundSize, 0, -GroundSize);
  glEnd();


  drawBlock({5.0, 5.0}, {-5.0, -5.0}, buildingTypes[0], randomPositions[0], 0.25);
  glLoadIdentity();

  drawBlock({15.0, 15.0}, {5.0, 5.0}, buildingTypes[1], randomPositions[1], 0.25);
  glLoadIdentity();

  drawBlock({5.0, -15.0}, {15.0, -5.0}, buildingTypes[2], randomPositions[2], 0.25);
  glLoadIdentity();

  drawBlock({-15.0, 5.0}, {-5.0, 15.0}, buildingTypes[3], randomPositions[3], 0.25);
  glLoadIdentity();

  drawBlock({-15.0, -15.0}, {-5.0, -5.0}, buildingTypes[4], randomPositions[4], 0.25);
  glLoadIdentity();

  glFlush();
  glutSwapBuffers();

  setProjection();
}

void mouseControl(int button, int state, int x, int y)
{
  switch (button)
  {
  case 3:
    if (state == GLUT_DOWN)
    {
      fieldOfView += 5.0f;
      printf("%f\n", fieldOfView);
    }
    break;
  case 4:
    if (state == GLUT_DOWN)
    {
      fieldOfView -= 5.0f;
      printf("%f\n", fieldOfView);
    }
    break;
  default:
    printf("Mouse clicked %d\n", button);
    break;
  }
}

void specialKeyControl(int key, int x, int y)
{
  float change = 5.0f;
  switch (key)
  {
  case GLUT_KEY_UP:
    eye.z += change;
    glutPostRedisplay();
    break;
  case GLUT_KEY_DOWN:
    eye.z -= change;
    glutPostRedisplay();
    break;
  case GLUT_KEY_LEFT:
    eye.x -= change;
    glutPostRedisplay();
    break;
  case GLUT_KEY_RIGHT:
    eye.x += change;
    glutPostRedisplay();
    break;
  default:
    printf("Special key pressed: %d\n", key);
    break;
  }
}

void keyboardControl(unsigned char key, int x, int y)
{
  switch (key)
  {
  default:
    printf("Key pressed: %d\n", key);
    break;
  }
}

void reshape(int w, int h)
{
  if (h == 0)
  {
    h = 1;
  }

  /* Set the view port */
  glViewport(0, 0, w, h);

  setProjection();
}

void init(void)
{
  srand(time(NULL));
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glShadeModel(GL_SMOOTH);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  for (int i = 0; i < 5; i++)
  {
    int nb = rand() % 3 + 1;
    for (int j = 0; j < nb; j++)
    {
      int bType = rand() % 3 + 1;
      buildingTypes[i].push_back(bType);
      int random_x = rand() % BlockSize;
      if ((random_x % 2) == 0)
        random_x *= -1;
      int random_z = rand() % BlockSize;
      if ((random_z % 2) == 0)
        random_z *= -1;
      randomPositions[i].push_back(Vector(random_x, random_z));
    }
  }
}

int main(int argc, char **argv)
{
  glutInit(&argc, argv);
  int initial_width = glutGet(GLUT_WINDOW_WIDTH) | WindowWidth;
  int initial_height = glutGet(GLUT_WINDOW_HEIGHT) | WindowHeight;

  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(initial_width, initial_height);
  glutCreateWindow(PROGRAM_TITLE);

  init();
  glutDisplayFunc(display);
  glutIdleFunc(display);
  glutReshapeFunc(reshape);

  glutMouseFunc(&mouseControl);
  glutSpecialFunc(&specialKeyControl);
  glutKeyboardFunc(&keyboardControl);

  glutMainLoop();
  return 0; /* We'll never be here.*/
}
