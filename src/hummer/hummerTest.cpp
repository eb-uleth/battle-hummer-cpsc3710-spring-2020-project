/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#define PROGRAM_TITLE "Team Gandolf - Battle Hummer"

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>

#include "Hummer.h"

GLint Width = 1200;
GLint Height = 900;
GLint WindowID;

Hummer hummer = Hummer();

float rotateSpeed = 0.5f;
bool rotate = true;

void display(void)
{
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(0.0, 1.0, 8.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glPushMatrix();

  glRotatef(hummer.angle, 0.0, 1.0, 0.0);
  glTranslatef(0.0, -2.20, 0.0);
  hummer.draw();

  glPopMatrix();
  glutSwapBuffers();

  hummer.update();
  hummer.angle += rotateSpeed * (rotate ? 1 : 0);
}

void CallBackKeyPressed(unsigned char key, int x, int y)
{
  int dummy = x;
  x = y;
  y = dummy;
  switch (key)
  {
  /* SPACE BAR */
  case 32:
    rotate = !rotate;
    glutPostRedisplay();
    break;
  default:
    break;
  }
}

void CallBackSpecialKeyPressed(int key, int x, int y)
{
  int dummy = x;
  x = y;
  y = dummy;
  switch (key)
  {
  case GLUT_KEY_UP:
    hummer.move(1);
    glutPostRedisplay();
    break;
  case GLUT_KEY_DOWN:
    hummer.move(-1);
    glutPostRedisplay();
    break;
  default:
    break;
  }
}

void CallBackResizeScene(int w, int h)
{
  if (h == 0)
    h = 1;

  glViewport(0, 0, w, h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0f, (GLfloat)w / (GLfloat)h, 0.1f, 100.0f);

  glMatrixMode(GL_MODELVIEW);
  Width = w;
  Height = h;
}

void MyInit()
{
  glClearColor(0.05f, 0.05f, 0.05f, 0.0f);

  glClearDepth(1.0);
  glDepthFunc(GL_LESS);

  glShadeModel(GL_SMOOTH);

  glEnable(GL_DEPTH_TEST);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  CallBackResizeScene(Width, Height);
}

int main(int argc, char **argv)
{
  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

  glutInitWindowSize(Width, Height);

  WindowID = glutCreateWindow(PROGRAM_TITLE);

  glutDisplayFunc(&display);

  glutKeyboardFunc(&CallBackKeyPressed);
  glutSpecialFunc(&CallBackSpecialKeyPressed);

  glutIdleFunc(&display);

  MyInit();

  glutMainLoop();
  return 1;
}
