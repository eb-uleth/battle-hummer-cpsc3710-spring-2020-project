/*
* CPSC 3710 Spring 2020
* Copyright 2019 Everett Blakley
* Everett Blakley <everett.blakley@uleth.ca>
*/

#include "Hummer.h"

void Hummer::draw(void)
{
  float theta;
  glColor3f(0.55, 0.55, 0.55);
  glBegin(GL_QUADS);
  // Left Side
  glVertex3f(-1.0, 1.5, -1.5);
  glVertex3f(-1.0, 0.65, -1.5);
  glVertex3f(-1.0, 0.65, 0.5);
  glVertex3f(-1.0, 1.5, 0.5);

  glVertex3f(-1.0, 1.5, -1.5);
  glVertex3f(-1.0, 1.5, -2.5);
  glVertex3f(-1.0, 1.05, -2.5);
  glVertex3f(-1.0, 1.05, -1.5);

  glVertex3f(-1.0, 1.5, -2.5);
  glVertex3f(-1.0, 1.5, -3.0);
  glVertex3f(-1.0, 0.75, -3.0);
  glVertex3f(-1.0, 0.65, -2.5);

  glVertex3f(-1.0, 1.5, 0.5);
  glVertex3f(-1.0, 1.05, 0.5);
  glVertex3f(-1.0, 1.05, 1.5);
  glVertex3f(-1.0, 1.5, 1.5);

  glVertex3f(-1.0, 1.5, 1.5);
  glVertex3f(-1.0, 0.75, 1.5);
  glVertex3f(-1.0, 0.75, 2.0);
  glVertex3f(-1.0, 1.5, 2.0);

  glVertex3f(-1.0, 1.5, 2.0);
  glVertex3f(-1.0, 0.75, 2.0);
  glVertex3f(-0.7, 0.75, 2.25);
  glVertex3f(-0.7, 1.4, 2.25);

  // Right Side
  glVertex3f(1.0, 1.5, -1.5);
  glVertex3f(1.0, 1.5, 0.5);
  glVertex3f(1.0, 0.65, 0.5);
  glVertex3f(1.0, 0.65, -1.5);

  glVertex3f(1.0, 1.5, -1.5);
  glVertex3f(1.0, 1.05, -1.5);
  glVertex3f(1.0, 1.05, -2.5);
  glVertex3f(1.0, 1.5, -2.5);

  glVertex3f(1.0, 1.5, -2.5);
  glVertex3f(1.0, 0.65, -2.5);
  glVertex3f(1.0, 0.75, -3.0);
  glVertex3f(1.0, 1.5, -3.0);

  glVertex3f(1.0, 1.5, 0.5);
  glVertex3f(1.0, 1.5, 1.5);
  glVertex3f(1.0, 1.05, 1.5);
  glVertex3f(1.0, 1.05, 0.5);

  glVertex3f(1.0, 1.5, 1.5);
  glVertex3f(1.0, 1.5, 2.0);
  glVertex3f(1.0, 0.75, 2.0);
  glVertex3f(1.0, 0.75, 1.5);

  glVertex3f(1.0, 1.5, 2.0);
  glVertex3f(0.7, 1.4, 2.25);
  glVertex3f(0.7, 0.75, 2.25);
  glVertex3f(1.0, 0.75, 2.0);

  //Front Bumper
  glVertex3f(-1.0, 1.5, 2.0);
  glVertex3f(-0.7, 1.40, 2.25);
  glVertex3f(0.7, 1.40, 2.25);
  glVertex3f(1.0, 1.5, 2.0);

  glVertex3f(-0.7, 1.40, 2.25);
  glVertex3f(-0.7, 0.75, 2.25);
  glVertex3f(0.7, 0.75, 2.25);
  glVertex3f(0.7, 1.40, 2.25);

  // Back Bumper
  glVertex3f(-1.0, 1.5, -3.0);
  glVertex3f(1.0, 1.5, -3.0);
  glVertex3f(1.0, 0.75, -3.0);
  glVertex3f(-1.0, 0.75, -3.0);

  // Roof (Back)
  glVertex3f(-1.0, 1.5, -3.0);
  glVertex3f(-0.75, 2.5, 0.0);
  glVertex3f(0.75, 2.5, 0.0);
  glVertex3f(1.0, 1.5, -3.0);

  // Roof (Front)
  glVertex3f(-1.0, 1.5, 2.0);
  glVertex3f(1.0, 1.5, 2.0);
  glVertex3f(0.75, 2.5, 0.0);
  glVertex3f(-0.75, 2.5, 0.0);
  glEnd();

  glBegin(GL_TRIANGLES);
  // Side Panel - Left
  glVertex3f(-0.75, 2.5, 0.0);
  glVertex3f(-1.0, 1.5, -3.0);
  glVertex3f(-1.0, 1.5, 2.0);

  // Side Panel - Right
  glVertex3f(0.75, 2.5, 0.0);
  glVertex3f(1.0, 1.5, 2.0);
  glVertex3f(1.0, 1.5, -3.0);
  glEnd();

  glColor3f(0.15, 0.15, 0.15);

  glBegin(GL_QUADS);
  // Front Window
  glVertex3f(-0.75 + 0.1, 2.5001, 0.05);
  glVertex3f(-0.91666 + 0.1, 1.834 - 0.125, 1.3333 + 0.25);
  glVertex3f(0.916666 - 0.1, 1.834 - 0.125, 1.3333 + 0.25);
  glVertex3f(0.75 - 0.1, 2.5001, 0.05);

  // Back Window
  glVertex3f(-1.0 + 0.1, 1.501, -3.0);
  glVertex3f(-0.83333 + 0.1, 2.1680, -1.0);
  glVertex3f(0.833333 - 0.1, 2.1680, -1.0);
  glVertex3f(1.0 - 0.1, 1.501, -3.0);

  // Left Window - Back Portion
  glVertex3f(-0.78145, 2.3765, 0.0);
  glVertex3f(-0.87025, 2.0235, -1.0);
  glVertex3f(-0.96890, 1.6265, -1.0);
  glVertex3f(-0.96890, 1.6265, 0.0);

  // Right Window - Back Portion
  glVertex3f(0.78145, 2.3765, 0.0);
  glVertex3f(0.96890, 1.6265, 0.0);
  glVertex3f(0.96890, 1.6265, -1.0);
  glVertex3f(0.87025, 2.0235, -1.0);
  glEnd();

  glBegin(GL_TRIANGLES);
  // Left Window - Front Portion
  glVertex3f(-0.78145, 2.3765, 0.0);
  glVertex3f(-0.96890, 1.6265, 0.0);
  glVertex3f(-0.96890, 1.6265, 1.33333);

  // Right Window - Front Portion
  glVertex3f(0.78145, 2.3765, 0.0);
  glVertex3f(0.96890, 1.6265, 1.33333);
  glVertex3f(0.96890, 1.6265, 0.0);
  glEnd();

  // Lights / License Plate
  glBegin(GL_QUADS);
  // Headlight Bar
  glColor3f(0.8, 0.9, 0.75);
  glVertex3f(-0.7, 1.4, 2.2505);
  glVertex3f(-0.7, 1.3, 2.2505);
  glVertex3f(0.7, 1.3, 2.2505);
  glVertex3f(0.7, 1.4, 2.2505);

  // Headlight Left
  glVertex3f(0.7, 0.90, 2.2505);
  glVertex3f(0.5, 0.90, 2.2505);
  glVertex3f(0.5, 0.80, 2.2505);
  glVertex3f(0.7, 0.80, 2.2505);

  // Headlight Right
  glVertex3f(-0.7, 0.90, 2.2505);
  glVertex3f(-0.7, 0.80, 2.2505);
  glVertex3f(-0.5, 0.80, 2.2505);
  glVertex3f(-0.5, 0.90, 2.2505);

  // Tailight Bar
  glColor3f(0.65, 0.1, 0.1);
  glVertex3f(-0.95, 1.3, -3.0005);
  glVertex3f(-0.95, 1.4, -3.0005);
  glVertex3f(0.95, 1.4, -3.0005);
  glVertex3f(0.95, 1.3, -3.0005);

  // License Plate - Red Border
  glColor3f(0.8, 0.2, 0.2);
  glVertex3f(0.25, 1.0, -3.0005);
  glVertex3f(0.25, 0.8, -3.0005);
  glVertex3f(-0.25, 0.8, -3.0005);
  glVertex3f(-0.25, 1.0, -3.0005);

  // License Plate - White Border
  glColor3f(0.9, 0.9, 0.9);
  glVertex3f(0.20, 0.975, -3.0010);
  glVertex3f(0.20, 0.8255, -3.0010);
  glVertex3f(-0.20, 0.825, -3.0010);
  glVertex3f(-0.20, 0.975, -3.0010);
  glEnd();

  glColor3f(0.8, 0.9, 0.75);
  glBegin(GL_TRIANGLES);
  // Headlight Bar - Left
  glVertex3f(-0.7, 1.4, 2.2505);
  glVertex3f(-1.0, 1.5, 2.0005);
  glVertex3f(-0.7, 1.3, 2.2505);

  // Headlight Bar - Right
  glVertex3f(1.0, 1.5, 2.0005);
  glVertex3f(0.7, 1.4, 2.2505);
  glVertex3f(0.7, 1.3, 2.2505);
  glEnd();

  glColor3f(0.15, 0.15, 0.15);

  // Highlight Lines
  glBegin(GL_LINES);
  glVertex3f(-1.001, 1.5, -3.0);
  glVertex3f(-1.001, 1.5, 2.0);

  glVertex3f(1.001, 1.5, -3.0);
  glVertex3f(1.001, 1.5, 2.0);

  glVertex3f(-1.0, 1.5, 2.0);
  glVertex3f(-1.0, 0.75, 2.0);

  glVertex3f(-1.0, 1.5, 2.0);
  glVertex3f(-0.7, 1.4, 2.25);

  glVertex3f(1.0, 1.5, 2.0);
  glVertex3f(0.7, 1.4, 2.25);

  glVertex3f(0.7, 1.4, 2.25);
  glVertex3f(0.7, 0.75, 2.25);

  glVertex3f(-0.7, 1.4, 2.25);
  glVertex3f(-0.7, 0.75, 2.25);

  glVertex3f(1.0, 1.5, 2.0);
  glVertex3f(1.0, 0.75, 2.0);

  glVertex3f(-1.0, 1.5, 2.0);
  glVertex3f(-0.75, 2.5, 0.0);

  glVertex3f(1.0, 1.5, 2.0);
  glVertex3f(0.75, 2.5, 0.0);

  glVertex3f(-0.75, 2.5, 0.0);
  glVertex3f(-1.0, 1.5, -3.0);

  glVertex3f(0.75, 2.5, 0.0);
  glVertex3f(1.0, 1.5, -3.0);

  glVertex3f(0.75, 2.5, 0.0);
  glVertex3f(-0.75, 2.5, 0.0);

  glVertex3f(1.0, 1.5, -3.0);
  glVertex3f(1.0, 0.75, -3.0);

  glVertex3f(-1.0, 1.5, -3.0);
  glVertex3f(-1.0, 0.75, -3.0);

  glVertex3f(1.0, 1.5, -3.0);
  glVertex3f(-1.0, 1.5, -3.0);
  glEnd();

  glPushMatrix();

  // Wheels
  glRotatef(90, 00, 1.0, 0.0);

  glColor3f(0.15, 0.15, 0.15);
  glTranslatef(-1.0, 0.7, 1.0);
  glutSolidTorus(0.15, 0.35, 10, 25);
  glColor3f(0.6, 0.6, 0.6);
  glBegin(GL_LINE_STRIP);
  for (theta = 0; theta < 360; theta = theta + 40)
  {
    glVertex3f(0.0, 0.0, 0.10);
    glVertex3f(0.30 * (cos(((theta + tireAngle) * 3.14) / 180)), 0.30 * (sin(((theta + tireAngle) * 3.14) / 180)), 0.1);
  }
  glEnd();

  glColor3f(0.15, 0.15, 0.15);
  glTranslatef(0, 0, -2.0);
  glutSolidTorus(0.15, 0.35, 10, 25);
  glColor3f(0.6, 0.6, 0.6);
  glBegin(GL_LINE_STRIP);
  for (theta = 0; theta < 360; theta = theta + 40)
  {
    glVertex3f(0.0, 0.0, -0.10);
    glVertex3f(0.30 * (cos(((theta + tireAngle) * 3.14) / 180)), 0.30 * (sin(((theta + tireAngle) * 3.14) / 180)), -0.1);
  }
  glEnd();

  glColor3f(0.15, 0.15, 0.15);
  glTranslatef(3.0, 0, 0);
  glutSolidTorus(0.15, 0.35, 10, 25);
  glColor3f(0.6, 0.6, 0.6);
  glBegin(GL_LINE_STRIP);
  for (theta = 0; theta < 360; theta = theta + 40)
  {
    glVertex3f(0.0, 0.0, -0.10);
    glVertex3f(0.0 + (0.30 * (cos(((theta + tireAngle) * 3.14) / 180))), 0.30 * (sin(((theta + tireAngle) * 3.14) / 180)), -0.1);
  }
  glEnd();

  glColor3f(0.15, 0.15, 0.15);
  glTranslatef(0, 0, 2.0);
  glutSolidTorus(0.15, 0.35, 10, 25);
  glColor3f(0.6, 0.6, 0.6);
  glBegin(GL_LINE_STRIP);
  for (theta = 0; theta < 360; theta = theta + 40)
  {
    glVertex3f(0.0, 0.0, 0.10);
    glVertex3f(0.30 * (cos(((theta + tireAngle) * 3.14) / 180)), 0.30 * (sin(((theta + tireAngle) * 3.14) / 180)), 0.1);
  }
  glEnd();

  // Antenna
  glColor3f(0.8, 0.8, 0.8);
  glTranslatef(0.0, 1.0, -1.00);
  glRotatef(-90.0, 1.0, 0.0, 0.0);
  gluCylinder(gluNewQuadric(), 0.25, 0.25, 1.5, 10, 25);

  glTranslatef(0.0, 0.0, 2.0);
  glScalef(0.5, 0.5, 0.5);
  glPushMatrix();

  glColor3f(0.0, 0.35, 0.95);
  glRotatef(antennaRotate, 0.0, 1.0, 1.0);
  glutSolidTorus(0.10, 1.0, 20, 45);

  glColor3f(0.1, 0.95, 0.1);
  glRotatef(outerAntennaRotate, 0.0, 1.0, 0.0);
  glutSolidTorus(0.10, 0.75, 15, 30);

  glColor3f(0.95, 0.9, 0.2);
  glRotatef(midAntennaRotate, 1.0, 0.0, 0.0);
  glutSolidTorus(0.10, 0.5, 10, 25);

  glColor3f(0.95, 0.1, 0.1);
  glRotatef(innerAntennaRotate, 1.0, 1.0, 1.0);
  glScalef(0.3, 0.3, 0.3);
  glutSolidOctahedron();

  glPopMatrix();
  glPopMatrix();

  outerAntennaRotate += outerAntennaSpeed;
  midAntennaRotate += midAntennaSpeed;
  innerAntennaRotate += innerAntennaSpeed;
  antennaRotate += antennaSpeed;
}

