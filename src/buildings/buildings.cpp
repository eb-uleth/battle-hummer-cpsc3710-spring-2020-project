#define PROGRAM_TITLE "Team Gandolf - Battle Hummer"

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>

#include "Building.h"

GLint Width = 1200;
GLint Height = 900;
GLint WindowID;

float buildingSpeed = 0.5;
float buildingRotate = 0.0;

// ***************************** REQUIRED TO WORK **************************

float cubeSpeed = 2.0;
float cubeRotate = 0.0;
float cylinderSpeed = 0.50;
float cylinderRotate = 0.0;

//***************************NO LONGER REQUIRED *********************************
// *************************** ONE MORE BELOW ***********************************

void display(void) {
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluLookAt(0.0, 5.0, 15.0, 0.0, 3.0, 0.0, 0.0, 1.0, 0.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glPushMatrix();
   
   // drawSquareBuilding();
   drawCylinderBuilding(cylinderRotate);
   // drawSphereBuilding();
   glPopMatrix();
   glPushMatrix();

   glTranslatef(-5.0, 0.0, 0.0);
   glRotatef(buildingRotate, 0.0, 1.0, 0.0);
   drawSquareBuilding();
   // drawCylinderBuilding();
   // drawSphereBuilding();
   glPopMatrix();
   glPushMatrix();

   glTranslatef(5.0, 0.0, 0.0);
   // drawSphereBuilding();
   // drawCylinderBuilding();
   drawSphereBuilding(cylinderRotate);

   glutSwapBuffers();

   buildingRotate = fmod(buildingRotate + buildingSpeed, 360.0);

   //********************** REQUIRED ************************************************
   cubeRotate = fmod(cubeRotate + cubeSpeed, 360.0);
   cylinderRotate = fmod(cylinderRotate + cylinderSpeed, 360.0);
   //********************************************************************************

}

void CallBackKeyPressed(unsigned char key, int x, int y) {
   switch (key) {
         case 32:
            buildingSpeed = 0.5;
            glutPostRedisplay();
            break;
         case 113:
            buildingSpeed = 0.0;
            glutPostRedisplay();
            break;
         default:
            break;
    }
}

void CallBackResizeScene(int w, int h) {
   if (h == 0)
      h = 1;

   glViewport(0, 0, w, h);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(45.0f,(GLfloat)w/(GLfloat)h,0.1f,100.0f);

   glMatrixMode(GL_MODELVIEW);
   Width  = w;
   Height = h;
}


void MyInit() {
   glClearColor(0.05f, 0.05f, 0.05f, 0.0f);

   glClearDepth(1.0);
   glDepthFunc(GL_LESS); 

   glShadeModel(GL_SMOOTH);
   glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   glEnable(GL_BLEND);

   glEnable(GL_DEPTH_TEST);

   glEnable(GL_CULL_FACE);
   glCullFace(GL_BACK);

   CallBackResizeScene(Width,Height);
}


int main(int argc, char **argv) {
   glutInit(&argc, argv);
   
   glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

   glutInitWindowSize(Width, Height);

   WindowID = glutCreateWindow(PROGRAM_TITLE);
   
   glutDisplayFunc(&display);

   glutKeyboardFunc(&CallBackKeyPressed);

   glutIdleFunc(&display);

   MyInit();

   glutMainLoop(); 
   return 1; 
}